/* Translations js allows interfacing with Javascript translations from Transifex for a normal website.
 *
 * How to use:
 * - setTranslationsFolder(folder)
 *     set the folder of the translations
 *     If your translations are in "js/translations" (default),
 *     Pass this: "js/translations"
 *     Changing it may trigger the observers.
 * - setDefaultLanguage(code)
 *     sets the default language
 *     This is one file to load. Default "en"
 * - setTranslationLanguageToBrowser()
 *     loads the translations given by the user's
 *     web browser.
 * - getBrowserLanguageCode()
 *     Return the language code of the browser.
 * - getTranslationLanguage()
 *     Return the language code such as "en", "de-de"
 * - setTranslationLanguage(code)
 *     set the translation language to code
 *     code should be in the form of "de" or "de-de" and
 *     match the file in the translations folder.
 * - notifyAboutNewTranslations(callback)
 *     Whenever the translations change, callback will be called.
 *     Note that if you load a language code such as "de-de",
 *     this may be called twice for "de" and "de-de".
 * - translate(key, default)
 *     Translate a key to the current language.
 *     If the key is not found, default is returned.
 *     If default is not given (translate(key)), key is returned.
 * - setTranslation(code, key, value)
 *     Set the translation of a key to a value in a language
 *     given by code.
 * - define(translations)
 *    used in the lollbak
 le files to translate key/value pairs.
 */

// lookup for language if no language is given
var DEFAULT_LANGUAGE_LOOKUP = ["en"];
var TRANSLATIONS_FOLDER = "js/translations";
// en: {key: value}, 'de-de': {...}
var translationsByLanguageCode = {};
// functions that are called when the translations change
var toNotifyAboutChangedTranslations = [];
var currentLanguageLookup = DEFAULT_LANGUAGE_LOOKUP;
// list of loaded language files
var loadedLanguageFiles = [];

function setTranslationsFolder(folder) {
  TRANSLATIONS_FOLDER = folder;
}

function setDefaultLanguage(code) {
  if (DEFAULT_LANGUAGE_LOOKUP[0] != code) {
    DEFAULT_LANGUAGE_LOOKUP = [code];
    var lookup = DEFAULT_LANGUAGE_LOOKUP.concat(currentLanguageLookup.slice(1, currentLanguageLookup.length));
    _setLanguageLookup(lookup);
  }
}

function setTranslation(code, key, value) {
  var translations = translationsByLanguageCode[code];
  if (translations == undefined) {
    translationsByLanguageCode[code] = translations = {};
  }
  translations[key] = value;
}

function getTranslationLanguage() {
  return currentLanguageLookup[currentLanguageLookup.length - 1];
}

/* Easy way to set the lookup chain of the translations.
 * - languageCode is either like "de" or like "de-de". 
 */
function setTranslationLanguage(languageCode) {
  var code = languageCode.split("-");
  var lookup = DEFAULT_LANGUAGE_LOOKUP.concat([code[0]]);
  if (code.length >= 2) {
    lookup.push(languageCode);
  } 
  _setLanguageLookup(lookup);
}

function _setLanguageLookup(lookup) {
  var lookupChanged = lookup.join(",") != currentLanguageLookup.join(",");
  currentLanguageLookup = lookup;
  currentLanguageLookup.forEach(_loadTranslationFileForLanguage);
  if (lookupChanged) {
    _notifyObserversAboutChangedTranslation();
  }
}

function setTranslationLanguageToBrowser() {
  setTranslationLanguage(getBrowserLanguageCode());
}

function getBrowserLanguageCode() {
    // example to match from the WebView of the app:
    // "Mozilla/5.0 (Linux; U; Android 2.3.5; de-de; GT-I9001 Build/GINGERBREAD) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1 | language: en"
    var languageMatch = navigator.userAgent.match(/\|\s*language:\s*(\S+)$/);
    var locale = languageMatch == null ? navigator.language : languageMatch[1];
    return locale;
}



/* Create a new language definition.
 * We assume that the file
 * - is a .js file
 * - has the form of either
 *    - <language>.js (en.js) or 
 *    - <language>-<country>.js (en-us.js)
 */
function define(translations) {
  var stack = new Error().stack;
  var matches = stack.matchAll("/?([-a-z]+)\.js");
  matches.next(); // this file is 0
  var match = matches.next().value;
  var languageCode = match[1];
  for (var key in translations) {
    if (Object.prototype.hasOwnProperty.call(translations, key)) {
      setTranslation(languageCode, key, translations[key]);
    }
  }
  _loadedTranslations(languageCode);
}

var _translationInfoMessages = [];
function _translationInfo(message) {
  if (!_translationInfoMessages.includes(message)) {
    _translationInfoMessages.push(message);
    console.log(message);
  }
}

/* translate a key to a translated value.
 * - key: a String
 * - defaultValue is optional, defaults to key and is returned if
 *   no translation is found
 */
function translate(key, defaultValue) {
  if (defaultValue == undefined) {
    defaultValue = key;
  }
  var lookupChain = _getLookupChain();
  for (var i = lookupChain.length - 1; i >= 0 ; i--) {
    var lookup = lookupChain[i];
    var translations = lookup.translations;
    var translation = translations[key];
    if (translation == undefined) {
      lookup.notFound(key);
      continue;
    }
    return translation;
  }
  return defaultValue;
}


function _getLookupChain() {
  var lookupChain = [];
  currentLanguageLookup.forEach(function(code) {
    var translations = translationsByLanguageCode[code];
    if (translations == undefined) {
      _translationInfo("INFO: translations for " + code + " not loaded.");
      return;
    }
    var lookup = {
      code: code,
      translations: translations,
      notFound: function(key) {
        return "INFO: key " + key + " is not translated to " + code;
      }
    }
    lookupChain.push(lookup);
  });
  if (lookupChain.length > 0) {
    lookupChain[0].onError = function(key) {
      return "ERROR: key " + key + " is not found in the " + lookupChain[0].code + " translations (default). It should be there. Make sure it is not misspelled.";
    };
  }
  return lookupChain;
}

function _loadTranslationFileForLanguage(languageCode) {
  var source = "js/translations/" + languageCode + ".js";
  if (loadedLanguageFiles.includes(source)) {
    _translationInfo("LANG: " + source + " already loaded.");
    return;
  }
  script = document.createElement("script");
  script.src = source;
  document.head.appendChild(script);
  loadedLanguageFiles.push(source);
}

function _loadedTranslations(languageCode) {
  _translationInfo("INFO: Loaded translations for " + languageCode);
  if (currentLanguageLookup.includes(languageCode)) {
    _notifyObserversAboutChangedTranslation()
  }
}

function _notifyObserversAboutChangedTranslation() {
    toNotifyAboutChangedTranslations.forEach(function (callback) {
        callback();
    });
}

function notifyAboutChangedTranslations(callback) {
  if (_getLookupChain().length != 0) {
    callback();
  }
  toNotifyAboutChangedTranslations.push(callback);
}
