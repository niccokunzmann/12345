/* Common functions used by all files.
 *
 */
 
var KEY_STARS = "stars-";
var KEY_LANGUAGE = "language";
var ATTR_TRANSLATION_KEY = "translate";
var USE_LOCAL_STORAGE = localStorage != undefined;

function getHashStorage() {
  var hash = document.location.hash;
  if (hash.length <= 1) {
    return {};
  }
  hash = hash[0] == "#" ? hash.slice(1, hash.length) : hash;
  return JSON.parse(hash);
}

function setKeyValue(key, value) {
  if (USE_LOCAL_STORAGE) {
    localStorage[key] = value;
  } else {
    var storage = getHashStorage();
    storage[key] = value;
    document.location.hash = "#" + JSON.stringify(storage);
    turnLocalLinksIntoStorageLinks();
  }
}
 
function getKeyValue(key, defaultValue) {
  var storage = USE_LOCAL_STORAGE ? localStorage : getHashStorage();
  var value = storage[key];
  if (value === undefined) {
    return defaultValue;
  }
  return value;
}

function getFilename() {
  var path = document.location.pathname.split("/");
  return path[path.length - 1];
}

function getThisLevelId() {
  var filename = getFilename();
  return filename.split(".")[0];
}

function setStarsForThisLevel(stars) {
  var levelId = getThisLevelId();
  console.log("Stars for " + levelId + ": " + stars);
  setKeyValue(KEY_STARS + levelId, stars + "");
}

function setStarsForAllLevels() {
  var stars = document.getElementsByClassName("stars");
  for (var i = 0; i < stars.length; i++) {
    var star = stars[i];
    for (var j = 0; j < star.classList.length; j++) {
      var cls = star.classList.item(j);
      var starsString = getKeyValue(KEY_STARS + cls, null);
      if (starsString != null) {
        star.innerText = starsString;
        break;
      }
    }
  }
}

window.addEventListener("load", function () {
  setStarsForAllLevels();
  notifyAboutChangedTranslations(translateWebsite);
  notifyAboutChangedTranslations(playIntroForThisLevel);
  initializeTranslationLanguage();
});

function translateWebsite() {
  var log = "  // translations for " + getFilename() + "\n";
  var kv = {};
  function logT(key, value) {
    if (!kv[key]) {
      kv[key] = true;
      var jsonKey = JSON.stringify(key);
      var jsonValue = JSON.stringify(value);
      log += "  " + jsonKey + ": " + jsonValue + ",\n";
    }
    return translate(key, value);
  }
  document.title = logT(document.title, document.title);
  var elements = document.getElementsByClassName("T");
  for (var i = 0; i < elements.length; i++) {
    var element = elements[i];
    var value = element.innerHTML;
    var key = value;
    if (element.hasAttribute(ATTR_TRANSLATION_KEY)) {
      key = element.getAttribute(ATTR_TRANSLATION_KEY);
    } else {
      element.setAttribute(ATTR_TRANSLATION_KEY, key);
    }
    var translatedText = logT(key, value);
    element.innerHTML = translatedText;
  }
  console.log(log);
  turnLocalLinksIntoStorageLinks();
}

function playIntroForThisLevel() {
  var levelId = getThisLevelId();
  var introId = levelId + "-intro";
  play(introId);
}

if (play == undefined) {
  var play = function (id) {
    console.log("Sound not loaded.")
  }
}

function initializeTranslationLanguage() {
  var language = getKeyValue(KEY_LANGUAGE, getBrowserLanguageCode());
  setTranslationLanguage(language);
}

function setAndRememberLanguage(languageCode) {
  setKeyValue(KEY_LANGUAGE, languageCode);
  setTranslationLanguage(languageCode);
}

function turnLocalLinksIntoStorageLinks() {
  var links = document.getElementsByTagName("a");
  for (var i = 0; i < links.length; i++) {
    turnLocalLinkintoStorageLink(links[i]);
  }
}

function turnLocalLinkintoStorageLink(link) {
  href = link.href.split("#")[0];
  if ((href.includes("http://") || href.includes("https://")) && !href.includes("://localhost") || !href.includes(".html")) {
    console.log("exclude", link.href);
    return;
  }
  var hash = document.location.hash[0] == "#" ? document.location.hash.slice(1, document.location.hash.length) : document.location.hash;
  console.log(link, href , hash);
  link.href = href + "#" + hash;
}
