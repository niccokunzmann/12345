# 12345 - Learn Counting

[![pipeline status](https://gitlab.com/niccokunzmann/12345/badges/master/pipeline.svg)](https://gitlab.com/niccokunzmann/12345/-/commits/master)

<center>**[visit the website](https://niccokunzmann.gitlab.io/12345/)**</center>

This game makes it fun to learn counting.

For programmers, it is also easy to contribute new levels.

## Contributing

You can open issues if something is not working out and give feedback.

### Translating

You can contribute [translations](https://www.transifex.com/mundraub-android/12345-learn-counting/) to the app.
If you like to add sound files in your language, either send them to us or 
add them to the `app/src/main/assets/12345/sound/` folder. 

## Development

It is pure HTML/CSS/JavaScript.
All the source code can be found in [app/src/main/assets/12345/](app/src/main/assets/12345/).

If you develop the game, please make a merge request so that we can see it, talk and help.

### New Levels

It is quite easy do create a new level.
All the source code can be found in [app/src/main/assets/12345/](app/src/main/assets/12345/).

- create pictures
- create a folder for the pitures
- store pictures in the folder that are small. I used maximum height/width of 400px and 61% quality.
- copy the file of an existing level
- edit the file to include the pictures
- add new sound files to the [sound/en/](app/src/main/assets/12345/sound/en/) folder
    - `level-10-intro.ogg` as your level introduction
    - `10.ogg` to say a number
- add the file to the [play.html](app/src/main/assets/12345/play.html) menu
- add new strings to the [js/translations/en.js](app/src/main/assets/12345/js/translations/en.js) file
- add your name to the about page

### Development Setup

In the folowing replace text like `THIS_TEXT` with something meaningful.

If you would like to contribute to the project, you can do the following:

1. [Fork the project](https://gitlab.com/niccokunzmann/12345/).
    This creates a copy of the project at `gitlab.com/YOUR_USERNAME/12345`.
2. Install [git](https://git-scm.com/downloads).
3. Clone the project.
    ```
    git clone https://gitlab.com/YOUR_USERNAME/12345.git
    ```
4. Change into the repository.
    ```
    cd 12345
    ```
5. In the project, add the original repository as source as this allows getting the latest changes.
    ```
    git remote add upstream https://gitlab.com/niccokunzmann/12345
    ```

Now, you are set and you can change the source code and give the changes to others.

### Uploading Contributions

1. Checkout a new branch for your changes. `git checkout -b BRANCH_NAME`
2. Do this for all you have in mind:
    1. Modify a file.
    2. See the changes `git status`
    3. Add the changes `git add PATH_TO_FILE_FROM_GIT_STATUS`
    4. See the added changes `git status`
    5. Create a new version with the changes `git commit -m"DESCRIPTION OF CHANGE"`
    6. Push the changes to GitHub `git push -u origin BRANCH_NAME` or if you did that already `git push`.
    7. If the original repository was modified during your work, you can update your branch. `git pull upstream BRANCH_NAME`
3. As soon as possible, [create a merge request](https://gitlab.com/niccokunzmann/12345/compare) for other to see that you are working on something.
4. We will discuss everything in the [merge requests](https://gitlab.com/niccokunzmann/12345/merges) or the [issues](https://gitlab.com/niccokunzmann/12345/issues).

## Update Translations

Install the [Transifex Command Line Client](https://github.com/transifex/transifex-client/) for Raspberry Pi and Android Phones.
([Android](https://github.com/niccokunzmann/transifex-client-armv7l-binary)).
You may be prompted to get an API token which you can get if you have an account.

1. Add the changelogs.
    ```
    bash ci/add_changelogs_to_transifex.sh
    git add .tx
    git commit -am"translate changelogs"
    ```
2. Push the translations.
    ```
    tx push --source
    ```
3. Update all translations.
    ```
    tx pull --all
    ```
4. Commit the changes.
    ```
    git add .
    git commit -am"update translations"
    git push
    ```

## New Release

To release a new version:

1. Edit `metadata/en/changelogs/<NO>.txt` to describe the changes.
2. Edit `metadata/en/full_description.txt` to include the new features.
3. Edit `app/build.gradle`, the `versionCode` and `versionName` variables.
4. Run these scripts to update the source:
    ```
    bash ci/generate_sound_file_index.sh
    ```
5. Update the translations as mentioned above.
6. Create a commit and push it on the  master branch.
    ```
    git checkout master
    git add .
    git status # check that all is right
    git diff   # check that all is right
    git commit -m"v1.3"
    ```
7. Create a commit for the version information.
    ```
    bash ci/create_version_info_js.sh
    git commit -am"update version info"
    ```
7. Create a tag named `v<versionName>` like `v1.3` if `versionName` is `"1.3"`:
    ```
    git tag v1.3 
    git push origin master
    git push origin v1.3
    ```
    F-Droid should now pick up the new version and release the app.


