#!/bin/bash

set -e

cd "`dirname \"$0\"`"
cd "../app/src/main/assets/"

echo "/* The link to the app is shown now. */" > 12345/css/download-app.css
